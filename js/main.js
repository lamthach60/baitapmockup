import { monAnService } from "./service/monAnService.js";
import { monAnController } from "./controller/monAnController.js";
let foodList = [];
let idFood = null;
let renderTable = (listMonAn) => {
  let contentHTML = "";
  for (let index = 0; index < listMonAn.length; index++) {
    let monAn = listMonAn[index];
    let contentTr = `<tr>
                        <td>
                        ${monAn.id}
                        </td>
                        <td>
                        ${monAn.name}
                        </td>
                        <td>
                        ${monAn.price}
                        </td>
                        <td>
                        ${monAn.description}
                        </td>
                        <td>
                        <button onclick="xoaMonAn(${monAn.id})" class="btn btn-success">Xóa</button>
                        </td>
                        <td>
                        <button class="btn btn-success" onclick="layThongTinMonAn(${monAn.id})">Sửa</button>
                        </td>
                    </tr>`;
    contentHTML = contentTr + contentHTML;
  }
  document.getElementById("tbody_content").innerHTML = contentHTML;
};
let renderDSService = () => {
  monAnService
    .showDanhSachMonAn()
    .then((res) => {
      console.log(res);
      foodList = res.data;
      renderTable(foodList);
    })
    .catch((err) => {
      console.log(err);
    });
};
renderDSService();
let themMoiMonAn = () => {
  let monAn = monAnController.layThongTinTuForm();
  monAnService
    .themMonAn(monAn)
    .then((res) => {
      console.log(res);
      renderDSService();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.themMoiMonAn = themMoiMonAn;

let xoaMonAn = (id) => {
  monAnService
    .deleteMonAn(id)
    .then((res) => {
      console.log(res);
      renderDSService();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaMonAn = xoaMonAn;

let layThongTinMonAn = (id) => {
  idFood = id;
  monAnService
    .layThongTinChiTietMonAn(idFood)
    .then((res) => {
      console.log(res);
      monAnController.showThongTinLenForm(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.layThongTinMonAn = layThongTinMonAn;

let capNhatMonAn = () => {
  let monAn = monAnController.layThongTinTuForm();
  let newMonAn = { ...monAn, id: idFood };
  monAnService
    .capNhatMonAn(newMonAn)
    .then((res) => {
      console.log(res);
      monAnController.showThongTinLenForm({
        name: "",
        price: "",
        description: "",
      });
      renderDSService();
    })
    .catch((err) => {
      console.log(err);
    });
};

window.capNhatMonAn = capNhatMonAn;
